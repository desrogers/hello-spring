package com.galvanize.hellospring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {
    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_noArgs_returnsHelloWorld() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello"))
        .andExpect(status().isOk())
        .andExpect(content().string("Hello World"));
    }

    @Test
    void sayHello_withName_returnsHelloName() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello?name=Noname"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Noname"));
    }

    @Test
    void computeSeries_withAddition_shouldReturnCorrectValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?operator=add&nums=1,1,1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }

    @Test
    void computeSeries_withSubtraction_shouldReturnCorrectValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?operator=sub&nums=1,1,1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("-1"));
    }

    @Test
    void countVowels_withNull_shouldReturnZero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/vowels"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }

    @Test
    void countVowels_withHello_shouldReturnZero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/vowels?query=hello"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("2"));
    }

    @Test
    void countVowels_withAllVowels_shouldReturnZero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/vowels?query=aeiou"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("5"));
    }

    @Test
    void replace_withNullQueryAndPattern_shouldReturn405() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/replace", "Hello World"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void replace_withQueryAndPattern_shouldReturnHelloWorld() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/replace?query=i&pattern=l").content("Heiio Worid"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));
    }
}
