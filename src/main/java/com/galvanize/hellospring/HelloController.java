package com.galvanize.hellospring;

import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String sayHello(@RequestParam(defaultValue = "World", required = false) String name) {
        return String.format("Hello %s", name);
    }

    @GetMapping("/compute")
    public String computeSeries(
            @RequestParam(required = true) String operator,
            @RequestParam(required = true) int[] nums
    ) {
        switch (operator) {
            case "add":
                int sum = 0;
                for (int i = 0; i < nums.length; i++) {
                    sum += nums[i];
                }
                return Integer.toString(sum);
            case "sub":
                int result = nums[0];
                for (int i = 1; i < nums.length; i++) {
                    result -= nums[i];
                }
                return Integer.toString(result);
            default:
                return "";
        }
    }

    @GetMapping("/vowels")
    public String countVowels(@RequestParam(defaultValue = "", required = false) String query) {
        int count = 0;
        Set<Character> vowels = new HashSet<>();
        vowels.add('a');
        vowels.add('e');
        vowels.add('i');
        vowels.add('o');
        vowels.add('u');
        for (int i = 0; i < query.length(); i++) {
            if (vowels.contains(query.charAt(i))) count += 1;
        }
        return Integer.toString(count);
    }

    @PostMapping("/replace")
    public String replace(@RequestBody String body, @RequestParam String query, @RequestParam String pattern) {
        return body.replace(query, pattern);
    }
}
